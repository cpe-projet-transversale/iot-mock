const axios = require('axios');

const API_SIMULATOR = "http://localhost:3000"
const API_EM = "http://localhost:3000"


function sync_sensors(){
  axios.get(`${API_SIMULATOR}/sensors`)
  .then(response => {

    response.data.forEach(sensor => {
      delete sensor.lat
      delete sensor.long

      axios.patch(`${API_EM}/sensors/${sensor.id}`).catch(error => {
        console.log(error.code);
      });

    });

    console.log(`Synchonized: ${response.data.length} sensors`)

  })
  .catch(error => {
    console.log(error);
  });

  setTimeout(sync_sensors, 1000);
}

sync_sensors()
